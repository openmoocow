/*
 * main.c
 *
 * The Top Level Source File
 *
 * (c) 2008-2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This file is part of OpenMooCow - accelerometer moobox simulator
 *
 * OpenMooCow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenMooCow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenMooCow.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <glib.h>
#include <SDL.h>
#include <unistd.h>

#include "types.h"
#include "mainwindow.h"
#include "accelerometers.h"

int main(int argc, char *argv[])
{
	MainWindow *mw;
	int graphics = 0;
	GThread *accel_thread;
	int finished = 0;

	g_thread_init(NULL);

	if ( gtk_init_check(&argc, &argv) ) {
		graphics = 1;
	}

	if ( SDL_Init(SDL_INIT_AUDIO) < 0 ) {
		fprintf(stderr, "Couldn't initialise SDL: %s\n",
							SDL_GetError());
		return 1;
	}

	/* Start the accelerometer thread */
	accel_thread = accelerometer_start(&finished);

	if ( graphics ) {
		/* Open the window */
		mw = mainwindow_open();
		if ( mw != NULL ) gtk_main();
	} else {
		while ( 1 ) {
			sleep(10);
		}
	}

	finished = 1;
	g_thread_join(accel_thread);
	SDL_Quit();

	return 0;
}
