/*
 * types.h
 *
 * Data types
 *
 * (c) 2008-2009 Thomas White <taw@bitwiz.org.uk>
 * (c) 2008 Joachim Breitner <mail@joachim-breitner.de>
 *
 * This file is part of OpenMooCow - accelerometer moobox simulator
 *
 * OpenMooCow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenMooCow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenMooCow.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TYPES_H
#define TYPES_H

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdint.h>
#include <SDL.h>

typedef enum {
	ACCEL_UNKNOWN,
	ACCEL_FREERUNNER,	/* Openmoko Neo Freerunner */
	ACCEL_HDAPS,	        /* Thinkpad HDAPS */
} AccelType;

typedef struct {

	int		fd;

	AccelType	type;

	int		x;
	int		y;
	int		z;

	/* Temporary values for use during reading */
	int		lx;
	int		ly;
	int		lz;

	/* Current state (driver dependent) */
	int		state;
	signed int	old_threshold;

} AccelHandle;

typedef struct {

	long		moo_len;
	long		moo_pos;
	Sint16		*moo_buf;
	int		aplay_fallback;

	unsigned int	mootex;

} AudioContext;

typedef struct {

	GtkWidget	*window;
	GtkWidget	*cow;

} MainWindow;

#endif /* TYPES_H */
