/*
 * accelerometers.h
 *
 * Accelerometer stuff
 *
 * (c) 2008-2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This file is part of OpenMooCow - accelerometer moobox simulator
 *
 * OpenMooCow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenMooCow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenMooCow.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ACCELEROMETERS_H
#define ACCELEROMETERS_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

#include "types.h"

extern AccelHandle *accelerometer_open(void);
extern void accelerometer_update(AccelHandle *accel);
extern GThread *accelerometer_start(int *finished);

#endif /* ACCELEROMETERS_H */
