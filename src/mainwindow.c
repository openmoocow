/*
 * mainwindow.c
 *
 * Main window
 *
 * (c) 2008-2009 Thomas White <taw@bitwiz.org.uk>
 *
 * This file is part of OpenMooCow - accelerometer moobox simulator
 *
 * OpenMooCow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OpenMooCow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OpenMooCow.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <stdlib.h>

#include "types.h"
#include "audio.h"

static gint mainwindow_closed(GtkWidget *widget, MainWindow *mw)
{
	gtk_main_quit();
	return 0;
}

static gint mainwindow_clicked(GtkWidget *widget, GdkEventButton *event,
				gpointer data)
{
	audio_trigger_moo();
	return 0;
}

MainWindow *mainwindow_open()
{
	MainWindow *mw;
	GtkWidget *eb;

	mw = malloc(sizeof(*mw));
	if ( mw == NULL ) return NULL;

	mw->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(mw->window), 240, 320);
	gtk_window_set_title(GTK_WINDOW(mw->window), "OpenMooCow");

	eb = gtk_event_box_new();
	mw->cow = gtk_image_new_from_file(PIXMAPDIR"/cow.png");
	gtk_container_add(GTK_CONTAINER(eb), mw->cow);
	gtk_container_add(GTK_CONTAINER(mw->window), eb);
	gtk_widget_add_events(GTK_WIDGET(eb), GDK_BUTTON_PRESS_MASK);
	gtk_misc_set_alignment(GTK_MISC(mw->cow), 0.5, 1.0);

	g_signal_connect(G_OBJECT(eb), "button_press_event",
					G_CALLBACK(mainwindow_clicked), mw);
	g_signal_connect(G_OBJECT(mw->window), "destroy",
					G_CALLBACK(mainwindow_closed), mw);
	gtk_widget_show_all(mw->window);

	return mw;
}
